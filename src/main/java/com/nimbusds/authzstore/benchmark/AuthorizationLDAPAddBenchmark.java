package com.nimbusds.authzstore.benchmark;


import java.util.ArrayList;
import java.util.Collection;

import com.unboundid.ldap.sdk.AddRequest;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.BasicAsyncResultListener;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionPool;
import com.unboundid.ldap.sdk.LDAPConnectionPoolStatistics;

import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.RefreshToken;


/**
 * Benchmarks LDAP add performance for OAuth 2.0 authorisation objects.
 *
 * @author Vladimir Dzhuvinov
 */
public class AuthorizationLDAPAddBenchmark {

	
	public static final String LDAP_HOST = "192.168.0.2";
	
	
	public static final int LDAP_PORT = 1389;
	
	
	public static final String LDAP_BIND_DN = "cn=Directory Manager";
	
	
	public static final String LDAP_BIND_PASSWORD = "secret";
	
	
	public static final String LDAP_BASE_DN = "ou=authorizations,dc=wonderland,dc=net";
	
	
	public static AddRequest generateAddRequest() {
		
		Collection<Attribute> attrs = new ArrayList<Attribute>();
		
		// Compose the entry DN
		RefreshToken refreshToken = new RefreshToken(12);
		String dn = "authzRefreshToken=" + refreshToken.getValue() + "," + LDAP_BASE_DN;
		
		attrs.add(new Attribute("objectClass", "top"));
		attrs.add(new Attribute("objectClass", "oauth2Authz"));
		
		attrs.add(new Attribute("authzAccessToken", new BearerAccessToken(16).getValue()));
		
		// 2^8 = 256 client IDs
		attrs.add(new Attribute("authzClientID", new ClientID(1).getValue()));
		
		// 2^16 = 65536 subjects
		attrs.add(new Attribute("authzSubject", new Subject(2).getValue()));
		
		attrs.add(new Attribute("authzScopeValue", "openid", "email", "profile"));
		
		attrs.add(new Attribute("authzIssuer", "https://c2id.com"));
		
		return new AddRequest(dn, attrs);
	}
	
	
	public static void main(String[] args)
		throws Exception {
		
		LDAPConnection connection = new LDAPConnection(LDAP_HOST, LDAP_PORT, LDAP_BIND_DN, LDAP_BIND_PASSWORD);
		System.out.println("Created initial LDAP connection: " + connection);
		
		LDAPConnectionPool connectionPool = new LDAPConnectionPool(connection, 4);
		System.out.println("Created LDAP connection pool: " + connectionPool);
		
		final int entryCount = 100000;
		System.out.println("Adding " + entryCount + " entries...");
		
		final long startTime = System.currentTimeMillis();
		
		BasicAsyncResultListener dummyResultListener = new BasicAsyncResultListener();
		
		for (int i=0; i < entryCount; i++) {
			
			if ((i % 2500) == 0)
				System.out.println("Added " + i + " entries");
			
			LDAPConnection pooledConnection = connectionPool.getConnection();
			
			pooledConnection.asyncAdd(generateAddRequest(), dummyResultListener);
			//pooledConnection.add(generateAddRequest());
			
			connectionPool.releaseConnection(pooledConnection);
		}
		
		
		final long endTime = System.currentTimeMillis();
		
		final long runTime = endTime - startTime;
		
		LDAPConnectionPoolStatistics stats = connectionPool.getConnectionPoolStatistics();
		
		System.out.println("LDAP connection pool stats: " + stats);
		
		System.out.println("Total run time: " + runTime + "ms = " + runTime / 1000 + "s");
		
		connectionPool.close();
	}
}
